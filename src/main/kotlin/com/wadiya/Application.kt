package com.wadiya

import io.ktor.server.engine.*
import io.ktor.server.netty.*
import com.wadiya.plugins.*

fun main() {
    embeddedServer(Netty, port = 8080, host = "0.0.0.0") {
        configureHeaders()
        configureAuthentication()
        configureRouting()
        configureMonitoring()
        configureSockets()
        configureSerialization()
    }.start(wait = true)
}

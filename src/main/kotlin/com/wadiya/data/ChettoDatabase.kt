package com.wadiya.data

import com.wadiya.data.collections.*
import org.litote.kmongo.coroutine.coroutine
import org.litote.kmongo.eq
import org.litote.kmongo.reactivestreams.KMongo

private val client = KMongo.createClient().coroutine
private val database = client.getDatabase("ChettoDatabase")
private val Users = database.getCollection<User>()
private val Items = database.getCollection<Item>()
private val Pieces = database.getCollection<Piece>()
private val PieceSets = database.getCollection<PieceSet>()

suspend fun registerUser(user: User): Boolean {
    return database.getCollection<User>().insertOne(user).wasAcknowledged()
}

suspend fun checkIfUserExists(email: String): Boolean {
    return Users.findOne(User::email eq email) != null
}

suspend fun checkPasswordForEmail(email: String, passwordToCheck: String): String? {
    val user = Users.findOne(User::email eq email)
    val actualPassword = user?.password ?: return null

    if (actualPassword == passwordToCheck){
        return user.id
    }

    return null
}

suspend fun getUserInventory(userID: String): List<Item>{
    return Items.find(Item::ownerID eq userID).toList()

}
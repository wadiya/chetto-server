package com.wadiya.data.collections

import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

data class Piece(
    override val name: String,
    override val ownerID: String,
    override val imageURI: String,
    val pieceType: PieceType,
    val pieceSetID: String,
    @BsonId
    override val id: String = ObjectId().toString(),
): Item()

enum class PieceType{
    Pawn, Knight, Bishop, Rook, Queen, King
}
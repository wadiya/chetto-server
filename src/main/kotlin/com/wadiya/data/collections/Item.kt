package com.wadiya.data.collections

import org.bson.codecs.pojo.annotations.BsonId
import org.bson.types.ObjectId

abstract class Item {
    abstract val id: String
    abstract val name: String
    abstract val ownerID: String
    abstract val imageURI: String
}

package com.wadiya.plugins

import com.wadiya.authentication.UserSession
import com.wadiya.data.checkPasswordForEmail
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.sessions.*

fun Application.configureAuthentication(){
    install(Sessions){
        cookie<UserIdPrincipal>("user_session"){
            cookie.path = "/"
            cookie.maxAgeInSeconds = 604800
        }
    }
    install(Authentication){
        basic {
            realm = "Chetto Server"
            validate { credentials ->
                val email = credentials.name
                val password = credentials.password
                val userId = checkPasswordForEmail(email, password)
                if (userId != null) {
                    UserSession(userId)
                } else null
            }
        }
    }
}
//        session<UserIdPrincipal>{
//            validate { session ->
//                //How to validate a session?
//                session
//            }
//            challenge {
//                call.respond(UnauthorizedResponse())
//            }
//        }
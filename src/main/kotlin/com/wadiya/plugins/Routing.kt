package com.wadiya.plugins

import com.wadiya.routes.inventoryRoutes
import com.wadiya.routes.loginRoute
import com.wadiya.routes.registerRoute
import io.ktor.routing.*
import io.ktor.http.*
import io.ktor.application.*
import io.ktor.response.*
import io.ktor.request.*

fun Application.configureRouting() {

    routing {
        registerRoute()
        loginRoute()
        inventoryRoutes()
    }
}

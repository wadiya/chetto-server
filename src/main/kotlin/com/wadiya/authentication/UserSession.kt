package com.wadiya.authentication

import io.ktor.auth.Principal

data class UserSession(
    val userId: String,
): Principal
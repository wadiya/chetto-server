package com.wadiya.routes

import com.wadiya.data.checkPasswordForEmail
import com.wadiya.data.requests.AccountRequest
import com.wadiya.data.responses.SimpleResponse
import io.ktor.application.*
import io.ktor.http.*
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.loginRoute() {
    route("/login") {
        post {
            val request = try {
                call.receive<AccountRequest>()
            } catch (e: ContentTransformationException) {
                call.respond(HttpStatusCode.BadRequest)
                return@post
            }

            val userId = checkPasswordForEmail(request.email, request.password)

            if (userId != null){
                call.respond(OK, SimpleResponse(true, "Login successful!"))
            } else {
                call.respond(OK, SimpleResponse(false, "The email or password is incorrect!"))

            }
        }
    }
}
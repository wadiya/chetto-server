package com.wadiya.routes

import com.wadiya.authentication.UserSession
import com.wadiya.data.collections.Item
import com.wadiya.data.getUserInventory
import io.ktor.application.*
import io.ktor.auth.*
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.response.*
import io.ktor.routing.*

fun Route.inventoryRoutes() {
    route("/inventory"){
        authenticate {
            get {
                val userId = call.principal<UserSession>()!!.userId
                val inventory: List<Item> = getUserInventory(userId)
                call.respond(OK, inventory)
            }
        }
    }

}
package com.wadiya.routes

import com.wadiya.data.checkIfUserExists
import com.wadiya.data.collections.User
import com.wadiya.data.registerUser
import com.wadiya.data.requests.AccountRequest
import com.wadiya.data.responses.SimpleResponse
import io.ktor.application.*
import io.ktor.http.HttpStatusCode.Companion.BadRequest
import io.ktor.http.HttpStatusCode.Companion.OK
import io.ktor.request.*
import io.ktor.response.*
import io.ktor.routing.*

fun Route.registerRoute(){
    route("/register"){
        post {
            val request = try {
                call.receive<AccountRequest>()
            } catch (e: ContentTransformationException) {
                call.respond(BadRequest)
                return@post
            }
            val userExists = checkIfUserExists(request.email)
            if (!userExists){
                if(registerUser(User(request.email, request.password))){
                    call.respond(OK, SimpleResponse(true, "Account creation successful!"))
                } else {
                    call.respond(OK, SimpleResponse(false, "Failed to create new user!"))
                }
            } else {
                call.respond(OK, SimpleResponse(false, "User with this email already exists"))
            }
        }
    }
}